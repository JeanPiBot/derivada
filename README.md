# OVA

Creación de un Objeto Virtual de Aprendizaje sobre el concepto de derivada como razón de cambio

## Getting Started

Esta dearrollado en Html5, Css3, Javascript y Bootstrap para utilizarlo solo debe dar clic en el Derivada.html, es un desarrollo web donde solo necesitarás  un navegador y coneccion a Internet. En algunos casos si tienes acceso al código fuente podras utilizarlo.

## Running the tests

No esta implementado aún.


## Built With

* [Moonlight](https://themewagon.com/themes/responsive-one-page-bootstrap-template/) - The web framework used


## Contributing

 Por favor para Contribuir en el desarrollo de este recurso debes dirigirte al siguiente repositorio [JbearP](https://gitlab.com/JbearP/derivada.git) Gracias por tus aportes.



## Authors

* **Ernesto Araujo Chavarro**
* **Jean Pierre Giovanni Arenas Ortiz** - *Initial work* - [JbearP](https://gitlab.com/JbearP?nav_source=navbar)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

